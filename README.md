# practicalnlpinpytoch
https://mlexplained.com/2019/01/30/an-in-depth-tutorial-to-allennlp-from-basics-to-elmo-and-bert/
https://github.com/keitakurita/Practical_NLP_in_PyTorch/



export KAGGLE_CONFIG_DIR=/exwindoz/home/juno/kaggleapi

kaggle kernels pull remotejob/practicalnlpinpytoch -p train/ -m

sed -i '/^#/ d' train/kagglerun.py

kaggle kernels push -p train/
kaggle kernels status remotejob/practicalnlpinpytoch

rm -rf output/*
kaggle kernels output remotejob/practicalnlpinpytoch -p output/

