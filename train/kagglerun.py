
from allennlp.modules.token_embedders import Embedding
from allennlp.predictors.sentence_tagger import SentenceTaggerPredictor
from allennlp.data.iterators import BasicIterator
from scipy.special import expit  # the sigmoid function
from tqdm import tqdm
from allennlp.data.iterators import DataIterator
from allennlp.training.trainer import Trainer
from allennlp.modules.seq2vec_encoders import PytorchSeq2VecWrapper
from allennlp.modules.text_field_embedders import BasicTextFieldEmbedder
from allennlp.modules.text_field_embedders import TextFieldEmbedder
from allennlp.models import Model
from allennlp.nn.util import get_text_field_mask
from allennlp.modules.seq2vec_encoders import Seq2VecEncoder, PytorchSeq2VecWrapper
import torch.nn as nn
from allennlp.data.iterators import BucketIterator
from allennlp.data.token_indexers import SingleIdTokenIndexer
from allennlp.data.tokenizers.word_splitter import SpacyWordSplitter
from allennlp.data.fields import TextField, MetadataField, ArrayField
from allennlp.data.dataset_readers import DatasetReader
from allennlp.data.vocabulary import Vocabulary
from allennlp.common.checks import ConfigurationError
import os

from pathlib import Path
from typing import *
import torch
import torch.optim as optim
import numpy as np
import pandas as pd
from functools import partial
from overrides import overrides

from allennlp.data import Instance
from allennlp.data.token_indexers import TokenIndexer
from allennlp.data.tokenizers import Token
from allennlp.nn import util as nn_util


class Config(dict):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        for k, v in kwargs.items():
            setattr(self, k, v)

    def set(self, key, val):
        self[key] = val
        setattr(self, key, val)


config = Config(
    testing=True,
    seed=1,
    batch_size=64,
    lr=3e-4,
    epochs=10000,
    hidden_sz=64,
    max_seq_len=200,  # necessary to limit memory usage
    max_vocab_size=100000,
)


USE_GPU = torch.cuda.is_available()

DATA_ROOT = Path("../input")

torch.manual_seed(config.seed)

label_cols = ["recognized"]


class JigsawDatasetReader(DatasetReader):
    def __init__(self, tokenizer: Callable[[str], List[str]] = lambda x: x.split(),
                 token_indexers: Dict[str, TokenIndexer] = None,
                 max_seq_len: Optional[int] = config.max_seq_len) -> None:
        super().__init__(lazy=False)
        self.tokenizer = tokenizer
        self.token_indexers = token_indexers or {
            "tokens": SingleIdTokenIndexer()}
        self.max_seq_len = max_seq_len

    @overrides
    def text_to_instance(self, tokens: List[Token], id: str = None,
                         labels: np.ndarray = None) -> Instance:
        sentence_field = TextField(tokens, self.token_indexers)
        fields = {"tokens": sentence_field}

        id_field = MetadataField(id)
        fields["id"] = id_field

        if labels is None:
            labels = np.zeros(len(label_cols))
        label_field = ArrayField(array=labels)
        fields["label"] = label_field

        return Instance(fields)

    @overrides
    def _read(self, file_path: str) -> Iterator[Instance]:
        df = pd.read_csv(file_path, sep='\t')
        if config.testing:
            df = df.head(1000)
        for i, row in df.iterrows():
            yield self.text_to_instance(
                [Token(x) for x in self.tokenizer(row["ask"])],
                row["ans"], row[label_cols].values,
            )


token_indexer = SingleIdTokenIndexer()


def tokenizer(x: str):
    return [w.text for w in
            SpacyWordSplitter(language='en_core_web_sm',
                              pos_tags=False).split_words(x)[:config.max_seq_len]]


reader = JigsawDatasetReader(
    tokenizer=tokenizer,
    token_indexers={"tokens": token_indexer}
)


train_ds, test_ds = (reader.read(DATA_ROOT / fname)
                     for fname in ["cleanoldfinbotconfilds.tsv", "cleanoldfinbotconfildsfortest.tsv"])
val_ds = None


len(train_ds)


print(train_ds[:10])


vars(train_ds[0].fields["tokens"])


vocab = Vocabulary.from_instances(
    train_ds, max_vocab_size=config.max_vocab_size)


iterator = BucketIterator(batch_size=config.batch_size,
                          sorting_keys=[("tokens", "num_tokens")],
                          )

iterator.index_with(vocab)

batch = next(iter(iterator(train_ds)))
# batch
# batch["tokens"]["tokens"]

# batch["tokens"]["tokens"].shape


class BaselineModel(Model):
    def __init__(self, word_embeddings: TextFieldEmbedder,
                 encoder: Seq2VecEncoder,
                 out_sz: int = len(label_cols)):
        super().__init__(vocab)
        self.word_embeddings = word_embeddings
        self.encoder = encoder
        self.projection = nn.Linear(self.encoder.get_output_dim(), out_sz)
        self.loss = nn.BCEWithLogitsLoss()

    def forward(self, tokens: Dict[str, torch.Tensor],
                id: Any, label: torch.Tensor) -> torch.Tensor:
        mask = get_text_field_mask(tokens)
        embeddings = self.word_embeddings(tokens)
        state = self.encoder(embeddings, mask)
        class_logits = self.projection(state)

        output = {"class_logits": class_logits}
        output["loss"] = self.loss(class_logits, label)

        return output


token_embedding = Embedding(num_embeddings=config.max_vocab_size + 2,
                            embedding_dim=300, padding_index=0)
word_embeddings: TextFieldEmbedder = BasicTextFieldEmbedder(
    {"tokens": token_embedding})


encoder: Seq2VecEncoder = PytorchSeq2VecWrapper(nn.LSTM(word_embeddings.get_output_dim(),
                                                        config.hidden_sz, bidirectional=True, batch_first=True))

model = BaselineModel(
    word_embeddings,
    encoder,
)


if USE_GPU:
    model.cuda()
else:
    model

batch = nn_util.move_to_device(batch, 0 if USE_GPU else -1)

tokens = batch["tokens"]
labels = batch


# tokens

mask = get_text_field_mask(tokens)
print("mask?")
print(mask)


embeddings = model.word_embeddings(tokens)
state = model.encoder(embeddings, mask)
class_logits = model.projection(state)
class_logits


model(**batch)


loss = model(**batch)["loss"]
print(loss)

loss.backward()


optimizer = optim.Adam(model.parameters(), lr=config.lr)


trainer = Trainer(
    model=model,
    optimizer=optimizer,
    iterator=iterator,
    train_dataset=train_ds,
    cuda_device=0 if USE_GPU else -1,
    num_epochs=config.epochs,
)


metrics = trainer.train()


def tonp(tsr): return tsr.detach().cpu().numpy()


class Predictor:
    def __init__(self, model: Model, iterator: DataIterator,
                 cuda_device: int = -1) -> None:
        self.model = model
        self.iterator = iterator
        self.cuda_device = cuda_device

    def _extract_data(self, batch) -> np.ndarray:
        out_dict = self.model(**batch)
        return expit(tonp(out_dict["class_logits"]))

    def predict(self, ds: Iterable[Instance]) -> np.ndarray:
        pred_generator = self.iterator(ds, num_epochs=1, shuffle=False)
        self.model.eval()
        pred_generator_tqdm = tqdm(pred_generator,
                                   total=self.iterator.get_num_batches(ds))
        preds = []
        with torch.no_grad():
            for batch in pred_generator_tqdm:
                batch = nn_util.move_to_device(batch, self.cuda_device)
                preds.append(self._extract_data(batch))
        return np.concatenate(preds, axis=0)


seq_iterator = BasicIterator(batch_size=64)
seq_iterator.index_with(vocab)


predictor = Predictor(model, seq_iterator, cuda_device=0 if USE_GPU else -1)
train_preds = predictor.predict(train_ds)
test_preds = predictor.predict(test_ds)


tagger = SentenceTaggerPredictor(model, reader)


print(tagger.predict("olen sinku ja olen puutsa en ole saanut pimpia pitkaan aikaan")) # 1
print(tagger.predict("niin mitä teet jos suutelisin sinua")) # 1
print(tagger.predict("rakkaani haluakko että suu mietä tulle pia seime")) # 1  - 1word
print(tagger.predict("kuinka monta tuntiaelisa sinulta menis mun henkiltä makaamiseen asti"))  #0
print(tagger.predict("okei elikkä tykkäät")) # 0
print(tagger.predict("nuorta luomutissistä tiukkareikäistä etsii hän")) # 0